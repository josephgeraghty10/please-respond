##################################
# please-respond.py
#
# Requirements:
# 
# Collect streaming RSVP data from Meetup.com and output aggregate information about the observed Event RSVPs.
#
# Program will connect to the Meetup.com and collect RSVP's to Meetup Events for a configurable time period (default 60 seconds)
#
# Ths following aggregate information should be calculated:
#   - Total # of RSVPs received
#   - Date of Event furthest into the future
#   - URL for the Event furthest into the future
#   - The top 3 number of RSVPs received per Event host-country
#
# Input Format
#   Program will connect to the Meetup.com RSVP HTTP data stream at http://stream.meetup.com/2/rsvps
#   Assume that the input files are correctly formmatted.  Error handling for invalid input files may be ommitted.
#
# Output Format
#   The ouput will specify the aggregate calculated information in a comma-delimited format.
#   The program will output to screen or console (and not to a file)
#
#   Example output
#   100,2019-04-17 12:00,https://www.meetup.com/UXSpeakeasy/events/258247836/,us,40,uk,18,jp,12
#
#
# Usage syntax:
# pyton please-respond.py <runtime>
#
# Example:
# pyton please-respond.py 60
#
##################################

import json
import requests
import sys
import time

# Initialize values
reponse = requests.get("https://stream.meetup.com/2/rsvps",stream=True)
results = {}


# Retrieve runtime and set end time
try:
  runtime = int(sys.argv[1])
except ValueError:
  runtime = 60
except IndexError:
  runtime = 60
end_time = time.time() + runtime

# Capture from Meeup stream and load into 'results' dictionary
for line in reponse.iter_lines():
  data = json.loads(line)
  event_date = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(data['event']['time']/1000))
  event_url = data['event']['event_url']
  group_country = data['group']['group_country']
  if results.has_key(event_date + "," + event_url):
    if results[event_date + "," + event_url].has_key(group_country):
      results[event_date + "," + event_url][group_country] = results[event_date + "," + event_url][group_country] + 1
    else:
      results[event_date + "," + event_url] = { group_country:1 }
  else:
    results[event_date + "," + event_url] = { group_country:1 }
  if time.time() > end_time:
     reponse.close()
     break

# Find top three country and aggregated totals for attendees
for key in results.keys():
  size = 0
  for subkey in results[key].keys():
    size = results[key][subkey] + size
    listofTuples = sorted(results[key].items() , reverse=True, key=lambda x: x[1])
    results[key]['top_three'] = listofTuples[0:3]
  results[key]['total'] = size

# Print results to screen
for key in results.keys():
  top_three_str = ""
  for elem in results[key]['top_three']:
    top_three_str = top_three_str +  elem[0] + "," + str(elem[1]) + ","
  print str(results[key]['total']) + "," + key + "," + top_three_str[:-1]
